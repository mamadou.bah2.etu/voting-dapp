// SPDX-License-Identifier: MIT
pragma solidity ^0.8.13;
import "@openzeppelin/contracts/access/Ownable.sol";

contract Voting is Ownable {
    constructor() Ownable() {}

    enum WorkflowStatus {
        RegisteringVoters,
        ProposalsRegistrationStarted,
        ProposalsRegistrationEnded,
        VotingSessionStarted,
        VotingSessionEnded,
        VotesTallied
    }

    struct Voter {
        bool isRegistered;
        bool hasVoted;
        uint256 votedProposalId;
        uint256 proposalsSubmitted;
        address delegate;
    }

    struct Proposal {
        address owner;
        string description;
        uint256 voteCount;
        bool isWinner;
    }

    mapping(address => Voter) public voters;
    Proposal[] public proposals;

    WorkflowStatus public status = WorkflowStatus.RegisteringVoters;
    uint256 public winningProposalId;
    uint256 public constant MAX_PROPOSALS_PER_VOTER = 3;

    event VoterRegistered(address voterAddress);
    event VoterRemoved(address voterAddress);
    event WorkflowStatusChange(
        WorkflowStatus previousStatus,
        WorkflowStatus newStatus
    );
    event ProposalRegistered(uint256 proposalId);
    event Voted(address voter, uint256 proposalId);
    event VoteWithdrawn(address voter, uint256 proposalId);
    event ProposalModified(uint256 proposalId, string newDescription);

    modifier onlyRegisteredVoter() {
        require(
            voters[msg.sender].isRegistered,
            "Only registered voters can perform this action"
        );
        _;
    }

    modifier hasNotVoted() {
        require(!voters[msg.sender].hasVoted, "Voter has already voted");
        _;
    }

    function registerVoter(address _voter) external onlyOwner {
        require(
            status == WorkflowStatus.RegisteringVoters,
            "Voters registration is not open"
        );
        require(!voters[_voter].isRegistered, "Voter is already registered");

        voters[_voter].isRegistered = true;
        emit VoterRegistered(_voter);
    }

    function removeVoter(address _voter) external onlyOwner {
        require(voters[_voter].isRegistered, "Voter is not registered");

        delete voters[_voter];
        emit VoterRemoved(_voter);
    }

    function delegateVote(address to) public {
        require(
            status == WorkflowStatus.VotingSessionStarted,
            "Voting session has not started yet"
        );
        require(
            voters[msg.sender].isRegistered,
            "You must be a registered voter to delegate"
        );
        require(!voters[msg.sender].hasVoted, "You have already voted");
        require(msg.sender != to, "Self-delegation prohibited");

        // Forward the delegation as long as the delegated voter has also delegated their vote
        while (voters[to].delegate != address(0)) {
            to = voters[to].delegate;

            // No looped delegation
            require(to != msg.sender, "Looped delegation detected");
        }

        voters[msg.sender].delegate = to;

        // If the delegatee has already voted, add the voter's vote to the proposal the delegatee voted for
        if (voters[to].hasVoted) {
            proposals[voters[to].votedProposalId].voteCount++;
        }
    }

    function startProposalsRegistration() external onlyOwner {
        require(
            status == WorkflowStatus.RegisteringVoters,
            "Cannot start proposals registration now"
        );
        status = WorkflowStatus.ProposalsRegistrationStarted;
        emit WorkflowStatusChange(
            WorkflowStatus.RegisteringVoters,
            WorkflowStatus.ProposalsRegistrationStarted
        );
    }

    function registerProposal(string calldata description)
        external
        onlyRegisteredVoter
    {
        require(
            status == WorkflowStatus.ProposalsRegistrationStarted,
            "Proposals registration is not open"
        );
        require(
            voters[msg.sender].proposalsSubmitted < MAX_PROPOSALS_PER_VOTER,
            "Voter has reached the maximum number of proposals"
        );

        proposals.push(
            Proposal({owner: msg.sender, description: description, voteCount: 0, isWinner: false})
        );

        voters[msg.sender].proposalsSubmitted++;
        emit ProposalRegistered(proposals.length - 1);
    }

    function modifyProposal(uint256 proposalId, string calldata newDescription)
        external
        onlyRegisteredVoter
    {
        require(
            status == WorkflowStatus.ProposalsRegistrationStarted,
            "Proposals registration is not open"
        );
        require(proposalId < proposals.length, "Invalid proposal ID");
        require(
            proposals[proposalId].owner == msg.sender,
            "You are not the owner of this proposal"
        );

        require(
            !voters[msg.sender].hasVoted,
            "Cannot modify proposal after voting"
        );

        proposals[proposalId].description = newDescription;
        emit ProposalModified(proposalId, newDescription);
    }

    function withdrawVote() external onlyRegisteredVoter {
        require(
            status == WorkflowStatus.VotingSessionStarted,
            "Voting session is not open"
        );
        require(voters[msg.sender].hasVoted, "Voter hasn't voted yet");

        uint256 proposalId = voters[msg.sender].votedProposalId;
        proposals[proposalId].voteCount--;

        voters[msg.sender].hasVoted = false;
        voters[msg.sender].votedProposalId = 0;

        emit VoteWithdrawn(msg.sender, proposalId);
    }

    function endProposalsRegistration() external onlyOwner {
        require(
            status == WorkflowStatus.ProposalsRegistrationStarted,
            "Cannot end proposals registration now"
        );
        status = WorkflowStatus.ProposalsRegistrationEnded;
        emit WorkflowStatusChange(
            WorkflowStatus.ProposalsRegistrationStarted,
            WorkflowStatus.ProposalsRegistrationEnded
        );
    }

    function getProposals() external view returns (address[] memory, string[] memory) {
        address[] memory proposalOwners = new address[](proposals.length);
        string[] memory proposalDescriptions = new string[](proposals.length);
        for (uint256 i = 0; i < proposals.length; i++) {
            proposalOwners[i] = proposals[i].owner;
            proposalDescriptions[i] = proposals[i].description;
        }
        return (proposalOwners, proposalDescriptions);
    }


    function startVotingSession() external onlyOwner {
        require(
            status == WorkflowStatus.ProposalsRegistrationEnded,
            "Cannot start voting session now"
        );
        status = WorkflowStatus.VotingSessionStarted;
        emit WorkflowStatusChange(
            WorkflowStatus.ProposalsRegistrationEnded,
            WorkflowStatus.VotingSessionStarted
        );
    }

    function vote(uint256[] memory proposalIds) public {
        require(
            status == WorkflowStatus.VotingSessionStarted,
            "Voting session has not started yet"
        );
        Voter storage sender = voters[msg.sender];
        require(
            sender.isRegistered && !sender.hasVoted,
            "Either you are not registered or you have already voted"
        );

        // Check if the vote is delegated
        address delegatee = sender.delegate;
        if (delegatee != address(0)) {
            require(
                !voters[delegatee].hasVoted,
                "The person you delegated your vote to has already voted"
            );
        }

        // Assign points to each proposal based on its ranking
        for (uint256 i = 0; i < proposalIds.length; i++) {
            proposals[proposalIds[i]].voteCount += 1;
        }

        sender.hasVoted = true;
        emit Voted(msg.sender, proposalIds[0]); // emit the event for the top-ranked proposal
    }

    function endVotingSession() external onlyOwner {
        require(
            status == WorkflowStatus.VotingSessionStarted,
            "Cannot end voting session now"
        );
        status = WorkflowStatus.VotingSessionEnded;
        emit WorkflowStatusChange(
            WorkflowStatus.VotingSessionStarted,
            WorkflowStatus.VotingSessionEnded
        );
    }

    function getTotalVotes() external view returns (uint256) {
        uint256 totalVotes = 0;
        for (uint256 i = 0; i < proposals.length; i++) {
            totalVotes += proposals[i].voteCount;
        }
        return totalVotes;
    }

    function tallyVotes() external onlyOwner {
        require(
            status == WorkflowStatus.VotingSessionEnded,
            "Cannot tally votes now"
        );

        uint256 winningVoteCount = 0;
        for (uint256 i = 0; i < proposals.length; i++) {
            if (proposals[i].voteCount > winningVoteCount) {
                winningVoteCount = proposals[i].voteCount;
                winningProposalId = i;
            }
        }

        // Single winner logic: Only the proposal with the highest vote count is the winner
        proposals[winningProposalId].isWinner = true;

        status = WorkflowStatus.VotesTallied;
        emit WorkflowStatusChange(
            WorkflowStatus.VotingSessionEnded,
            WorkflowStatus.VotesTallied
        );
    }

    function getWinners() external view returns (string[] memory) {
        require(
            status == WorkflowStatus.VotesTallied,
            "Votes have not been tallied yet"
        );

        uint256 winnerCount = 0;
        for (uint256 i = 0; i < proposals.length; i++) {
            if (proposals[i].isWinner) {
                winnerCount++;
            }
        }

        string[] memory winners = new string[](winnerCount);
        uint256 index = 0;
        for (uint256 i = 0; i < proposals.length && index < winnerCount; i++) {
            if (proposals[i].isWinner) {
                winners[index] = proposals[i].description;
                index++;
            }
        }

        return winners;
    }
}