let contractAbi;

window.addEventListener('load', async () => {
    try {
        const response = await fetch('../build/contracts/Voting.json');
        const contractJson = await response.json();
        contractAbi = contractJson.abi;
    } catch (error) {
        console.error("Erreur lors du chargement de l'ABI:", error);
        alert("Erreur lors du chargement de l'ABI.");
        return;
    }

    if (typeof window.ethereum !== 'undefined') {
        const provider = new ethers.providers.Web3Provider(window.ethereum);

        await window.ethereum.request({ method: 'eth_requestAccounts' });

        const accounts = await provider.listAccounts();
        if (accounts.length === 0) {
            console.error("Aucun compte trouvé. Veuillez connecter un compte dans MetaMask.");
            return;
        }

        const signer = provider.getSigner();
        const contractAddress = "0x6E4fc2777C3c67F08a2AF1160649c75AC54a5F4D";
        const contract = new ethers.Contract(contractAddress, contractAbi, signer);

        window.ethereum.on('accountsChanged', async function (accounts) {
            console.log("Changement de compte détecté : ", accounts);
            await displayAllProposals(); 
            location.reload();
        });


        document.getElementById('registerVoter').addEventListener('click', async () => {
            const voterAddressInput = document.getElementById('voterAddressInput');
            const voterAddress = voterAddressInput.value;
            if (!voterAddress || !ethers.utils.isAddress(voterAddress)) {
                displayErrorAboveElement(voterAddressInput, "Veuillez entrer une adresse Ethereum valide.");
                return;
            }

            try {
                const tx = await contract.registerVoter(voterAddress);
                await tx.wait();
                displaySuccessAboveElement(voterAddressInput, "Électeur enregistré avec succès!");
            } catch (error) {
                const errorMessage = handleContractError(error);
                console.error("Erreur:", errorMessage);
                displayErrorAboveElement(voterAddressInput, errorMessage);
            }
        });


        document.getElementById('removeVoter').addEventListener('click', async () => {
            const removeVoterInput = document.getElementById('removeVoterAddressInput');
            const voterAddressToRemove = removeVoterInput.value;

            if (!voterAddressToRemove || !ethers.utils.isAddress(voterAddressToRemove)) {
                displayErrorAboveElement(removeVoterInput, "Veuillez entrer une adresse Ethereum valide.");
                return;
            }

            try {
                const tx = await contract.removeVoter(voterAddressToRemove);
                await tx.wait();
                displaySuccessAboveElement(removeVoterInput, "Électeur supprimé avec succès!");
            } catch (error) {
                const errorMessage = handleContractError(error);
                console.error("Erreur:", errorMessage);
                displayErrorAboveElement(removeVoterInput, errorMessage);
            }
        });

        const startProposalsButton = document.getElementById('startProposals');
        startProposalsButton.addEventListener('click', async () => {
            try {
                const tx = await contract.startProposalsRegistration();
                await tx.wait();
                displaySuccessAboveElement(startProposalsButton, "Enregistrement des propositions commencé!");
            } catch (error) {
                const errorMessage = handleContractError(error);
                console.error("Erreur:", errorMessage);
                displayErrorAboveElement(startProposalsButton, errorMessage);
            }
        });


        document.getElementById('modifyProposal').addEventListener('click', async () => {
            const modifyProposalInput = document.getElementById('modifyProposalIdInput');
            const modifyDescInput = document.getElementById('modifyProposalDescInput');

            const proposalId = modifyProposalInput.value;
            const newDescription = modifyDescInput.value;

            if (!proposalId || !newDescription) {
                displayErrorAboveElement(modifyProposalInput, "Veuillez fournir un ID de proposition et une nouvelle description.");
                return;
            }

            try {
                const tx = await contract.modifyProposal(proposalId, newDescription);
                await tx.wait();
                displaySuccessAboveElement(document.getElementById('modifyProposal'), "Proposition modifiée avec succès!")
            } catch (error) {
                const errorMessage = handleContractError(error);
                console.error("Erreur:", errorMessage);
                displayErrorAboveElement(modifyProposalInput, errorMessage);
            }
        });

        document.getElementById('submitProposal').addEventListener('click', async () => {
            const proposalInput = document.getElementById('proposalInput');

            const proposal = proposalInput.value;
            if (!proposal) {
                displayErrorAboveElement(proposalInput, "Veuillez entrer une proposition.");
                return;
            }

            try {
                const tx = await contract.registerProposal(proposal);
                await tx.wait();
                displaySuccessAboveElement(proposalInput, "Proposition soumise avec succès!")
            } catch (error) {
                const errorMessage = handleContractError(error);
                console.error("Erreur:", errorMessage);
                displayErrorAboveElement(proposalInput, errorMessage);
            }
        });

        const endProposalsButton = document.getElementById('endProposals');
        endProposalsButton.addEventListener('click', async () => {

            try {
                const tx = await contract.endProposalsRegistration();
                await tx.wait();
                displaySuccessAboveElement(endProposalsButton, "Enregistrement des propositions terminé!")
            } catch (error) {
                const errorMessage = handleContractError(error);
                console.error("Erreur:", errorMessage);
                displayErrorAboveElement(endProposalsButton, errorMessage);
            }
        });


        document.getElementById('voteForProposal').addEventListener('click', async () => {
            const voteProposalIdInput = document.getElementById('voteProposalIdInput');

            const proposalIdsString = voteProposalIdInput.value;
            if (!proposalIdsString) {
                displayErrorAboveElement(voteProposalIdInput, "Veuillez fournir les IDs des propositions.");
                return;
            }

            const proposalIds = proposalIdsString.split(',').map(id => parseInt(id.trim()));

            try {
                const tx = await contract.vote(proposalIds);
                await tx.wait();
                displaySuccessAboveElement(voteProposalIdInput, "Vous avez voté avec succès pour les propositions!")
            } catch (error) {
                const errorMessage = handleContractError(error);
                console.error("Erreur:", errorMessage);
                displayErrorAboveElement(voteProposalIdInput, errorMessage);
            }
        });


        const startVotingButton = document.getElementById('startVoting');
        startVotingButton.addEventListener('click', async () => {
            try {
                const tx = await contract.startVotingSession();
                await tx.wait();
                displaySuccessAboveElement(startProposalsButton, "Session de vote commencée!")
                displayAllProposals();
            } catch (error) {
                const errorMessage = handleContractError(error);
                console.error("Erreur:", errorMessage);
                displayErrorAboveElement(startVotingButton, errorMessage);
            }
        });

        const withdrawVoteButton = document.getElementById('withdrawVote');
        withdrawVoteButton.addEventListener('click', async () => {
            try {
                const tx = await contract.withdrawVote();
                await tx.wait();
                displaySuccessAboveElement(withdrawVoteButton, "Vote retiré avec succès!")
            } catch (error) {
                const errorMessage = handleContractError(error);
                console.error("Erreur:", errorMessage);
                displayErrorAboveElement(withdrawVoteButton, errorMessage);
            }
        });

        const endVotingButton = document.getElementById('endVoting');
        endVotingButton.addEventListener('click', async () => {
            try {
                const tx = await contract.endVotingSession();
                await tx.wait();
                console.log(contract.status)
                displaySuccessAboveElement(endVotingButton, "Session de vote terminée!")
            } catch (error) {
                const errorMessage = handleContractError(error);
                console.error("Erreur:", errorMessage);
                displayErrorAboveElement(endVotingButton, errorMessage);
            }
        });

        const tallyVotesButton = document.getElementById('tallyVotesButton');
        tallyVotesButton.addEventListener('click', async () => {
            try {
                const status = await contract.status();
                console.log(status);
                await contract.tallyVotes();
                displaySuccessAboveElement(tallyVotesButton, "Les votes ont été comptés !")
            } catch (error) {
                const errorMessage = handleContractError(error);
                console.error("Erreur:", errorMessage);
                displayErrorAboveElement(tallyVotesButton, errorMessage);
            }
        });

        const showWinnersButton = document.getElementById('showWinnersButton');
        showWinnersButton.addEventListener('click', async () => {
            try {
                const winnersDescriptions = await contract.getWinners();
                const winnersList = document.getElementById('winnersList');
                winnersList.innerHTML = '';

                winnersDescriptions.forEach((description) => {
                    const listItem = document.createElement('li');
                    listItem.textContent = `Proposition gagnant: ${description}`;
                    winnersList.appendChild(listItem);
                });
                displaySuccessAboveElement(showWinnersButton, "Le gagnant a été affichés !")
            } catch (error) {
                const errorMessage = handleContractError(error);
                console.error("Erreur:", errorMessage);
                displayErrorAboveElement(showWinnersButton, errorMessage);
            }
        });

        // Afficher toutes les propositions pour le vote
        async function displayAllProposals() {
            try {
                const [proposalOwners, proposalDescriptions] = await contract.getProposals();
                const proposalsListElement = document.getElementById('proposalsList');
                proposalsListElement.innerHTML = '';

                proposalDescriptions.forEach((description, index) => {
                    const proposalElement = document.createElement('div');
                    proposalElement.classList.add('proposal-item');

                    const ownerElement = document.createElement('span');
                    ownerElement.classList.add('proposal-owner');
                    ownerElement.innerHTML = `<b>Propriétaire : </b> ${proposalOwners[index]}`;

                    const descriptionElement = document.createElement('span');
                    descriptionElement.classList.add('proposal-description');
                    descriptionElement.innerHTML = `<b>Proposition : </b> ${description}`;

                    proposalElement.appendChild(ownerElement);
                    proposalElement.appendChild(descriptionElement);

                    proposalsListElement.appendChild(proposalElement);
                });
            } catch (error) {
                console.error("Erreur lors de la récupération des propositions:", error);
                alert("Erreur lors de l'affichage des propositions. Veuillez réessayer.");
            }
        }
    } else {
        alert("Veuillez installer MetaMask ou utiliser un navigateur compatible avec Ethereum.");
    }

});

function handleContractError(error) {
    if (!error) return "Une erreur inattendue s'est produite. Veuillez réessayer.";

    if (error.message.includes("user denied")) {
        return "Transaction annulée. Vous avez refusé la transaction dans votre portefeuille.";
    }

    if (error.data) {
        const errorReason = error.data['0x08c379a0'];
        if (errorReason) {
            return `Erreur lors de l'exécution du contrat : ${errorReason.reason}. Veuillez vérifier vos actions et réessayer.`;
        } else if (typeof error.data === 'string' && error.data.includes("revert")) {
            return "Une erreur s'est produite lors de l'exécution du contrat. Veuillez vérifier vos actions et réessayer.";
        } else if (typeof error.data === 'string' && error.data.includes("out of gas")) {
            return "La transaction a échoué par manque de gaz. Augmentez la limite de gaz et réessayez.";
        } else {
            return `${error.data.data.reason}`;
        }
    }
}

function displayErrorAboveElement(element, errorMessage) {

    removeExistingErrorMessages(element)

    const errorDiv = document.createElement('div');
    errorDiv.className = 'error-message';
    errorDiv.textContent = errorMessage;

    element.parentNode.insertBefore(errorDiv, element);

    setTimeout(() => {
        errorDiv.remove();
    }, 5000);
}

function displaySuccessAboveElement(element, successMessage) {

    removeExistingErrorMessages(element)
    const successDiv = document.createElement('div');
    successDiv.className = 'success-message';
    successDiv.textContent = successMessage;

    successDiv.style.color = 'green';
    successDiv.style.border = '1px solid green';
    successDiv.style.padding = '5px';
    successDiv.style.borderRadius = '5px';
    successDiv.style.marginTop = '10px';
    successDiv.style.marginBottom = '10px';

    element.parentNode.insertBefore(successDiv, element);

    setTimeout(() => {
        successDiv.remove();
    }, 5000);
}


function removeExistingErrorMessages(element) {
    const existingError = element.parentNode.querySelector('.error-message');
    if (existingError) {
        existingError.remove();
    }
}


