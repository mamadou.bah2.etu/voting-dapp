# Voting Dapp Bah Mamadou-Saliou

Voici le lien vers la [demo sur youtube](https://youtu.be/AQFJy1W2GRg).

## Fonctionnalités

### Gestion des Comptes
- Détecte automatiquement le changement de compte et rafraîchit l'interface pour le compte actuel.
- Prise en charge complète de MetaMask pour une interaction fluide.

### Enregistrement des électeurs
- Permet aux administrateurs d'ajouter des électeurs.
- Fournit une validation pour s'assurer que les adresses Ethereum sont valides

### Suppression des électeurs
- Permet aux administrateurs de supprimer des électeurs.

### Gestion des propositions
- Commencer et terminer l'enregistrement des propositions.
- Soumettre de nouvelles propositions pour le vote.
- Modifier des propositions existantes.
- Afficher la liste complète des propositions pour le vote.
- Nombre de proposition limité a 3 par personne.

### Session de vote
- Démarrer et terminer une session de vote.
- Voter pour des propositions spécifiques.
- Retirer un vote si l'utilisateur change d'avis.
- Tally et affichez les votes après la fin de la session de vote.
- Affichez les propositions gagnantes après le dépouillement des votes.
- On peut transmettre son vote a quelqu'un d'autre, je l'ai ajouté sur le contract mais pas sur la dapp

### Messages d'erreur et de réussite
- Fournit des messages d'erreur et de réussite clairs pour guider l'utilisateur à chaque étape.